import React, { Component } from "react";
import { connect } from "react-redux";
import { TAI, XIU, CHOOSE_OPTION } from "../redux/constant/xucXacConstant";

let styles = {
  btn: {
    width: 150,
    height: 150,
    fontSize: 50,
  },
};

class XucXac extends Component {
  renderXucXac = () => {
    return this.props.mangXucXac.map((item) => {
      return (
        <img
          alt="true"
          src={item.img}
          style={{ width: "100px", margin: "30px" }}
        ></img>
      );
    });
  };
  render() {
    return (
      <div className="d-flex justify-content-between container">
        <button
          className="btn btn-danger"
          style={styles.btn}
          onClick={() => {
            this.props.handleChooseOption(TAI);
          }}
        >
          Tài
        </button>
        <div>{this.renderXucXac()}</div>
        <button
          className="btn btn-dark"
          style={styles.btn}
          onClick={() => {
            this.props.handleChooseOption(XIU);
          }}
        >
          Xỉu
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleChooseOption: (option) => {
      dispatch({ type: CHOOSE_OPTION, payload: option });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
