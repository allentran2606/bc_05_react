import { numberReducer } from "./numberReducer";
import { combineReducers } from "redux";

export const rootReducer_Demo_Mini = combineReducers({ numberReducer });
