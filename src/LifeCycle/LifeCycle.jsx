import React, { Component } from "react";
import Header from "./Header";

export default class LifeCycle extends Component {
  state = {
    like: 1,
  };
  componentDidMount() {
    // console.log(`LIFE_CYCLE  componentDidMount`);
    // Chỉ được chạy 1 lần duy nhất (sau khi render chạy)
    // Thường để gọi api - login
  }

  handlePlusLike = () => {
    // console.log(`Clicked`);
    this.setState({ like: this.state.like + 1 });
  };

  shouldComponentUpdate(nextProps, nextState) {
    // console.log(`currentState: `, this.state);
    // console.log(`nextState: `, nextState);
    if (nextState.like === 5) {
      return false;
    }
    return true;
    // Mặc định sẽ return true
  }

  render() {
    // console.log(`LIFE_CYCLE  render`);
    return (
      <div className="p-5 bg-primary text-white">
        {this.state.like < 3 && <Header />}
        {/* <Header /> */}
        <p>LifeCycle</p>
        <span className="display-4">{this.state.like}</span>
        <button onClick={this.handlePlusLike} className="btn btn-success">
          Like
        </button>
      </div>
    );
  }
}
