import React, { Component } from "react";
import UserForm from "./UserForm";
import UserTable from "./UserTable";

export default class Ex_Form extends Component {
  state = {
    userList: [
      {
        id: 1,
        name: "Alice",
        password: "123",
      },
    ],
    userEdited: null,
  };

  handleAddUser = (newUser) => {
    let cloneUserList = [...this.state.userList, newUser];
    this.setState({ userList: cloneUserList });
  };

  handleUserRemove = (userId) => {
    let index = this.state.userList.findIndex((item) => {
      return item.id === userId;
    });
    if (index !== 1) {
      let cloneUserList = [...this.state.userList];
      cloneUserList.splice(index, 1);
      this.setState({ userList: cloneUserList });
    }
  };

  handleEditUser = (value) => {
    this.setState({ userEdited: value });
  };

  render() {
    return (
      <div className="container py-5">
        <UserForm
          handleAddUser={this.handleAddUser}
          userEdited={this.state.userEdited}
        />
        <UserTable
          handleEditUser={this.handleEditUser}
          handleUserRemove={this.handleUserRemove}
          userList={this.state.userList}
        />
      </div>
    );
  }
}
