import React, { Component } from "react";

export default class DetailShoe extends Component {
  render() {
    return (
      <div className="row">
        <img className="col-4" alt="true" src={this.props.detail.image}></img>
        <div className="col-8">
          <p>{this.props.detail.name}</p>
          <p>{this.props.detail.price}</p>
          <p>{this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}
