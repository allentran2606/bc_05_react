import React, { Component } from "react";

export default class CardItem extends Component {
  render() {
    return (
      <div className="card text-left h-100">
        <img
          style={{
            height: "75%",
            objectFit: "cover",
          }}
          className="card-img-top"
          src={this.props.movie.hinhAnh}
          alt="true"
        />
        <div className="card-body">
          <h4 className="card-title">{this.props.movie.tenPhim}</h4>
        </div>
      </div>
    );
  }
}
