import React, { Component } from "react";
import { dataMovie } from "./dataMovie";
import CardItem from "./CardItem";

export default class RenderWithMap extends Component {
  renderMovieList = () => {
    return dataMovie.map((item) => {
      return (
        <div className="col-3 p-2">
          <CardItem movie={item} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="row container mx-auto">{this.renderMovieList()}</div>
    );
  }
}
