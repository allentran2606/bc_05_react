import React, { Component } from "react";
import UserInfo from "./UserInfo";

export default class DemoProps extends Component {
  renderTitle = () => {
    return <h1>Nice to meet you</h1>;
  };
  handleClickMe = () => {
    console.log(`Yes`);
  };
  render() {
    let user = {
      name: `Bob`,
      gmail: `bob@gmail.com`,
      sayHello: function () {
        console.log(`Siuuuu`);
      },
    };
    return (
      <div>
        <h2>DemoProps</h2>
        <button onClick={this.handleClickMe} className="btn btn-success">
          Click me
        </button>
        <UserInfo data={user} renderTitle={this.renderTitle} />
      </div>
    );
  }
}
