import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  state = {
    isLogin: false,
  };
  handleLogin = () => {
    this.setState({
      isLogin: true,
    });
  };
  handleLogout = () => {
    this.setState({
      isLogin: false,
    });
  };

  renderContent = () => {
    if (this.state.isLogin) {
      // Đã đăng nhập
      return (
        <div>
          <p>Hi Alice</p>
          <button className="btn btn-danger" onClick={this.handleLogout}>
            Logout
          </button>
        </div>
      );
    } else {
      // Chưa đăng nhập
      return (
        <div>
          <p>Welcome. Please login</p>
          <button className="btn btn-success" onClick={this.handleLogin}>
            Login
          </button>
        </div>
      );
    }
  };
  render() {
    return <div>{this.renderContent()}</div>;
  }
}
