import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cartData.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button>-</button>
            {item.number}
            <button>+</button>
          </td>
          <td>
            <img src={item.image} alt="true" style={{ width: "80px" }}></img>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}
