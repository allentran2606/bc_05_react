import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    return (
      <div className="col-lg-3 col-md-4 col-sm-6 p-2">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={this.props.data.image}
            alt="true"
          />
          <div className="card-body">
            <h4 className="card-title">{this.props.data.name}</h4>
            <p className="card-text">{this.props.data.shortDescription}</p>
            <button
              className="btn btn-primary mr-3"
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleViewDetail(this.props.data);
              }}
              // Gọi hàm handleChangeDetail bên Ex_Shoe_Shop
              // handleViewDetail(this.props.data) sẽ thay đổi nguyên cục dataShoe[0] bên Ex_Shoe_Shop
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}
