import React, { Component } from "react";

export default class UserInfo extends Component {
  render() {
    // console.log(this.props);
    let { data } = this.props;
    console.log(data.sayHello);
    return (
      <div>
        <h2>Username: {this.props.name}</h2>
        <h2>Gmail: {this.props.gmail} </h2>
        {this.props.renderTitle()}
        <h2>Hello: {data.sayHello()}</h2>
      </div>
    );
  }
}
