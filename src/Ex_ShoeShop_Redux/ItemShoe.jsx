import React, { Component } from "react";
import { connect } from "react-redux";
import { ADD_TO_CART } from "./redux/constants/shoeConstant";
import { VIEW_DETAIL } from "./redux/constants/shoeConstant";

class ItemShoe extends Component {
  render() {
    return (
      <div className="col-lg-3 col-md-4 col-sm-6 p-2">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={this.props.data.image}
            alt="true"
          />
          <div className="card-body">
            <h4 className="card-title">{this.props.data.name}</h4>
            <p className="card-text">{this.props.data.shortDescription}</p>
            <button
              className="btn btn-primary mr-3"
              onClick={() => {
                this.props.addToCart(this.props.data);
              }}
            >
              Buy
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.viewDetail(this.props.data);
              }}
            >
              Detail
            </button>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    shoeArr: state.shoeReducer.shoeArr,
    detail: state.shoeReducer.detail,
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      });
    },

    viewDetail: (shoe) => {
      dispatch({
        type: VIEW_DETAIL,
        payload: shoe,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemShoe);
