import { nanoid } from "nanoid";
import React, { Component } from "react";

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
  }

  state = {
    user: {
      name: "",
      password: "",
    },
  };

  handleUserSubmit = () => {
    let newUser = { ...this.state.user };
    newUser.id = nanoid(5);
    this.props.handleAddUser(newUser);
  };

  handleGetUserForm = (e) => {
    let { value, name: key } = e.target;
    let cloneUser = { ...this.state.user };
    cloneUser[key] = value;
    this.setState({ user: cloneUser }, () => {
      console.log(this.state.user);
    });
  };

  componentWillReceiveProps(nextProps) {
    // nextProps = this.props
    if (nextProps.userEdited != null) {
      this.setState({ user: nextProps.userEdited });
    }
  }

  render() {
    return (
      <div>
        <div className="form-group">
          <input
            type="text"
            onChange={this.handleGetUserForm}
            value={this.state.user.name}
            name="name"
            className="form-control"
            aria-describedby="helpId"
            placeholder="Username"
          />
        </div>
        <div className="form-group">
          <input
            type="text"
            onChange={this.handleGetUserForm}
            value={this.state.user.password}
            name="password"
            className="form-control"
            aria-describedby="helpId"
            placeholder="Password"
          />
        </div>
        <button className="btn btn-warning" onClick={this.handleUserSubmit}>
          Add user
        </button>
      </div>
    );
  }
}
