import { CHOOSE_OPTION, PLAY_GAME } from "../constant/xucXacConstant";
let initialState = {
  mangXucXac: [
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucSac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  soLuotThang: 0,
  soLuotChoi: 0,
};

export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let newMangXucXac = state.mangXucXac.map((item) => {
        let randomNumber = Math.floor(Math.random() * 6 + 1);
        console.log(randomNumber, `\n`);
        return {
          img: `./imgXucSac/${randomNumber}.png`,
          giaTri: randomNumber,
        };
      });
      return { ...state, mangXucXac: newMangXucXac };
    }
    case CHOOSE_OPTION: {
      return { ...state, luaChon: payload };
    }
    default:
      return state;
  }
};
