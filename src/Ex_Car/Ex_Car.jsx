import React, { Component } from "react";

export default class Ex_Car extends Component {
  state = {
    img: "./img/CarBasic/products/black-car.jpg",
  };
  handleChangeColor = (color) => {
    this.setState({
      img: `./img/CarBasic/products/${color}-car.jpg`,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <img className="col-3" src={this.state.img} alt="true"></img>
          <div>
            <button
              className="btn btn-dark"
              onClick={() => {
                this.handleChangeColor(`black`);
              }}
            >
              Black
            </button>
            <button
              className="btn btn-danger mx-5"
              onClick={() => {
                this.handleChangeColor(`red`);
              }}
            >
              Red
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.handleChangeColor(`silver`);
              }}
            >
              Silver
            </button>
          </div>
        </div>
      </div>
    );
  }
}
