import React, { Component } from "react";
import ItemShoe from "./ItemShoe";

export default class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.shoeArr.map((e) => {
      return (
        <ItemShoe
          handleViewDetail={this.props.handleChangeDetail}
          handleAddToCart={this.props.handleAddToCart}
          data={e}
        />
        // Gán giá trị của BIẾN handleChangeDetail bên Ex_ShoeShop vào BIẾN handleViewDetail
        // Đây là bước trung gian giúp onclick button sẽ trỏ tới function ở Ex_Shoe_Shop
      );
    });
  };
  render() {
    return <div className="row container mx-auto">{this.renderListShoe()}</div>;
  }
}
