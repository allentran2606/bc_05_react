import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { Provider } from "react-redux";
import { createStore } from "redux";
// import { rootReducer_Demo_Mini } from "./Ex_ShoeShop/Demo_Mini_Redux/redux/reducer/rootReducer";
import { rootReducer_XucXac } from "./Ex_Tai_Xiu/redux/reducer/rootReducer_XucXac";

const store = createStore(
  rootReducer_XucXac,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
