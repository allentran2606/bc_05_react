import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME } from "../redux/constant/xucXacConstant";

class KetQua extends Component {
  render() {
    return (
      <div className="text-center">
        <button className="btn btn-warning" onClick={this.props.handlePlayGame}>
          Play game
        </button>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({ type: PLAY_GAME });
    },
  };
};

export default connect(null, mapDispatchToProps)(KetQua);
