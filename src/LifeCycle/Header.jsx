import React, { PureComponent } from "react";

export default class Header extends PureComponent {
  componentDidMount() {
    let time = 10;
    this.myCountDown = setInterval(() => {
      console.log(`Countdown`, time--);
    }, 1000);
  }

  render() {
    console.log("HEADER  render");
    return <div className="p-5 bg-dark text-white">Header</div>;
  }

  componentWillUnmount() {
    console.log("HEADER componentWillUnmount");
    clearInterval(this.myCountDown);
  }
}

// PureComponent hạn chế render không cần thiết
