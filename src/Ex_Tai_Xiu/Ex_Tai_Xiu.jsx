import React, { Component } from "react";
import KetQua from "./KetQua/KetQua";
import XucXac from "./XucXac/XucXac";
import bg_game from "../assets/bgGame.png";
import "./game.css";

export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bg_game})`,
          height: `100vh`,
          width: `100vw`,
        }}
        className="font_game pt-5"
      >
        <XucXac />
        <KetQua />
      </div>
    );
  }
}
