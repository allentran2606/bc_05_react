import React, { Component } from "react";
import Header from "./Header";
import Navigation from "./Navigation";
import Content from "./Content";
import Footer from "./Footer";

export default class Ex_Layout extends Component {
  render() {
    return (
      <div className="text-white">
        <img src="" alt="" />
        <Header />
        <div className="row">
          <Navigation />
          <Content />
        </div>
        <Footer />
      </div>
    );
  }
}
