import React, { Component } from "react";
import { connect } from "react-redux";

class DetailShoe extends Component {
  render() {
    return (
      <div className="row">
        <img className="col-4" alt="true" src={this.props.detail.image}></img>
        <div className="col-8">
          <p>{this.props.detail.name}</p>
          <p>{this.props.detail.price}</p>
          <p>{this.props.detail.description}</p>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    detail: state.shoeReducer.detail,
  };
};

export default connect(mapStateToProps)(DetailShoe);
