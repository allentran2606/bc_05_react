import React, { Component } from "react";
import { connect } from "react-redux";

class Cart extends Component {
  renderTbody = () => {
    return this.props.gioHang.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <button className="btn btn-danger">-</button>
            {item.number}
            <button className="btn btn-success">+</button>
          </td>
          <td>
            <img src={item.image} alt="true" style={{ width: "80px" }}></img>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Image</th>
          </tr>
        </thead>
        <tbody>{this.renderTbody()}</tbody>
      </table>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    gioHang: state.shoeReducer.cart,
  };
};

export default connect(mapStateToProps)(Cart);
