import React, { Component } from "react";
import { connect } from "react-redux";
import ItemShoe from "./ItemShoe";

class ListShoe extends Component {
  renderListShoe = () => {
    return this.props.list.map((e) => {
      return (
        <ItemShoe data={e} />
        // Gán giá trị của BIẾN handleChangeDetail bên Ex_ShoeShop vào BIẾN handleViewDetail
        // Đây là bước trung gian giúp onclick button sẽ trỏ tới function ở Ex_Shoe_Shop
      );
    });
  };

  render() {
    return <div className="row container mx-auto">{this.renderListShoe()}</div>;
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.shoeArr,
  };
};

export default connect(mapStateToProps)(ListShoe);
