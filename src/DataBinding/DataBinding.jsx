import React, { Component } from "react";

export default class DataBinding extends Component {
  imgSrc = "https://movienew.cybersoft.edu.vn/hinhanh/trainwreck.jpg";
  renderDescription = () => {
    return <div>Hello to Alice</div>;
  };
  render() {
    let username = `Alice`;
    return (
      <div>
        <div
          style={{ width: "200px", height: "500px", backgroundColor: "red" }}
          className="card text-left"
        >
          <img className="card-img-top" src={this.imgSrc} alt="this is allen" />
          <div className="card-body">
            <h4 className="card-title">{username}</h4>
            <p className="card-text">{this.renderDescription()}</p>
          </div>
        </div>
      </div>
    );
  }
}
