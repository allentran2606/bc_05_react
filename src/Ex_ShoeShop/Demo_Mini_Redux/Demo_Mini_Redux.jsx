import React, { Component } from "react";
import { connect } from "react-redux";

class Demo_Mini_Redux extends Component {
  render() {
    return (
      <div>
        <button
          className="btn btn-danger"
          onClick={() => {
            this.props.handleGiamSoLuong(5);
          }}
        >
          Giảm
        </button>
        <span>{this.props.soLuong}</span>
        <button
          className="btn btn-success"
          onClick={this.props.handleTangSoLuong}
        >
          Tăng
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangSoLuong: () => {
      dispatch({
        type: "TANG_SO_LUONG",
      });
    },
    handleGiamSoLuong: (value) => {
      dispatch({
        type: "GIAM_SO_LUONG",
        payload: value,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo_Mini_Redux);
