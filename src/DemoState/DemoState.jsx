import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    like: 0,
  };
  handleAddLike = () => {
    // this.state.like = 10;
    // setState: bất đồng bộ
    this.setState(
      {
        like: this.state.like + 1,
      },
      () => {
        console.log("Thành công: ", this.state.like);
      }
    );
  };
  render() {
    return (
      <div>
        <p className="display-4">{this.state.like}</p>
        <button className="btn btn-primary" onClick={this.handleAddLike}>
          Add like
        </button>
      </div>
    );
  }
}
