import React, { Component } from "react";
import Cart from "./Cart";
import { dataShoe } from "./dataShoe";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: dataShoe,
    detail: dataShoe[0],
    cart: [],
  };

  handleChangeDetail = (value) => {
    this.setState({ detail: value });
  };

  handleAddToCart = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = this.state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    if (index === -1) {
      let cartItem = { ...shoe, number: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div className="container">
        <Cart cartData={this.state.cart} />
        <ListShoe
          handleChangeDetail={this.handleChangeDetail}
          // Gán giá trị function handleChangeDetail vào BIẾN handleChangeDetail
          shoeArr={this.state.shoeArr}
          handleAddToCart={this.handleAddToCart}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
