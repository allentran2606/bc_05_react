import "./App.css";
import Ex_Form from "./Ex_Form/Ex_Form";
// import LifeCycle from "./LifeCycle/LifeCycle";
// import Ex_Tai_Xiu from "./Ex_Tai_Xiu/Ex_Tai_Xiu";
// import Ex_ShoeShop_Redux from "./Ex_ShoeShop_Redux/Ex_ShoeShop_Redux";
// import Ex_ShoeShop from "./Ex_ShoeShop/Ex_ShoeShop";
// import Ex_Car from "./Ex_Car/Ex_Car";
// import DemoState from "./DemoState/DemoState";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import DataBinding from "./DataBinding/DataBinding";
// import DemoProps from "./DemoProps/DemoProps";
// import RenderWithMap from "./RenderWithMap/RenderWithMap";
// import Ex_Layout from "./Ex_Layout/Ex_Layout";
// import logo from "./logo.svg";
// import DemoClass from "./DemoComponent/DemoClass";
// import DemoFunction from "./DemoComponent/DemoFunction";
// import ConditionalRendering from "./ConditionalRendering/ConditionalRendering";

function App() {
  return (
    <div className="App">
      {/* <Ex_Layout /> */}
      {/* <RenderWithMap /> */}
      {/* <DataBinding /> */}
      {/* <DemoProps /> */}
      {/* <RenderWithMap /> */}
      {/* <DemoState /> */}
      {/* <Ex_Car /> */}
      {/* <ConditionalRendering /> */}
      {/* <Ex_ShoeShop /> */}
      {/* <Ex_ShoeShop_Redux /> */}
      {/* <Ex_Tai_Xiu /> */}
      {/* <LifeCycle /> */}
      <Ex_Form />
    </div>
  );
}

export default App;
