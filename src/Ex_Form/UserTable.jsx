import React, { Component } from "react";

export default class UserTable extends Component {
  renderUserTable = () => {
    return this.props.userList.map((item) => {
      return (
        <tr>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.password}</td>
          <td>
            <button
              className="btn btn-danger mr-3"
              onClick={() => {
                this.props.handleUserRemove(item.id);
              }}
            >
              Delete
            </button>
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.handleEditUser(item);
              }}
            >
              Edit
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <table className="table mt-5">
        <thead>
          <th>ID</th>
          <th>Name</th>
          <th>Password</th>
          <th>Action</th>
        </thead>
        <tbody>{this.renderUserTable()}</tbody>
      </table>
    );
  }
}
